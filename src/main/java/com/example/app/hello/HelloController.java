package com.example.app.hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.Map;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
        return "Hello World 4!";
    }

    @RequestMapping("/version")
    public String version() throws URISyntaxException, IOException {
        URL version = getClass().getClassLoader().getResource("VERSION");
        Path path = Paths.get(version.toURI());
        return Files.readAllLines(path).get(0);
    }
    @RequestMapping("/environment")
    public String environment() throws SocketException, UnknownHostException {
        StringBuilder sb = new StringBuilder();
        sb.append(InetAddress.getLocalHost().getHostAddress());
        sb.append("<br/>");
        Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
        while (networkInterfaces.hasMoreElements()) {
            NetworkInterface n = networkInterfaces.nextElement();
            Enumeration ee = n.getInetAddresses();
            while (ee.hasMoreElements())
            {
                InetAddress i = (InetAddress) ee.nextElement();
                sb.append(i.getHostAddress());
                sb.append("<br/>");
            }
        }
        for (Map.Entry p: System.getProperties().entrySet()) {
            sb.append(p.getKey());
            sb.append("=");
            sb.append(p.getValue());
            sb.append("<br/>");
        }
        return sb.toString();
    }
}